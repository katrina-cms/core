<?php
	class core_controller extends core_object {
		
		static protected	$is_ajax		= false;	// флаг AJAX-запроса
		static protected	$user_ip		= null;		// IP-адрес пользователя
		protected			$src_path		= null;		// путь до директории с ресурсами модуля
		protected			$module_id	= 0;			// ID модуля, которому соответствует модуль
		
		public function __construct() {
			if(preg_match('/^(?P<module>.+)_.+_controller$/i', get_class($this), $m)) {
				// формирование пути до директории с ресурсами
				$this->src_path	= MODULES.$m['module'].DS.'src'.DS;
				
				// получаем ID модуля по его имени
				$this->module_id = $this->main->get_module_id_by_name($m['module']);
			}
		}
		
		public function __before() {
			self::$is_ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']));
			self::$user_ip = $_SERVER['REMOTE_ADDR'];
		}
		
		public function __after() {
			if(self::$is_ajax) {
				die(json_encode(array(
					'status'	=> 'ok',
					'content'	=> $this->_tpl->get('content')
				)));
			}
		}
		
		// перекрывается потомками
		public function __assign() {
		
		}
		
		final public function __assign_all() {
			$this->__assign();
			
			// подключаем JS и CSS по-умолчанию
			$this->_js->assign($this->_url->get('module').'/'.$this->_url->get('action'));
			$this->_css->assign($this->_url->get('module').'/'.$this->_url->get('action'));
		}
		
	}
?>