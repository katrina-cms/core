<?php
	class core_application extends core_object {
		
		private $uri		= null;
		private $controller	= null;
		
		private $module		= 'main';
		private $action		= 'index';
		private $params		= array();
		
		public function __construct() {
			$this->analyze();
			$this->pre_globals();
			$this->start();
		}
		
		/**
		 * Ìåòîä ïîëó÷åíèÿ ïîäãîòîâëåííîãî ê îáðàáîòêå URI-àäðåñà
		 */
		public function getURI() {
			$uri = $_SERVER['REQUEST_URI']; 
			
			// îòñåêàåì $_GET
			$uri = explode('?', $uri);
			$uri = $uri[0];
			
			// îòñåêàåì îêîíå÷íûå ñëåøè
			if(preg_match('/\/$/', $uri)) {
				$uri = substr($uri, 0, -1);
				$uri = substr($uri, 1);
			} else {
				$uri = substr($uri, 1);
			}
			
			return explode('/', $uri);
		}
		
		public function analyze() {
			$uri = $this->getURI();
			
			// отпределяем и отсекаем путь к админке
			if(!empty($uri[0]) && $uri[0] == $this->_config->get('admin_dir', 'system')) {
				array_shift($uri);
				self::$app = 'adm';
			}
			
			// подключаем кастомный контроллер
			if(!empty($uri[0])) {
				$this->controller = self::load_class($uri[0].'_'.self::$app.'_controller');
				if(!empty($this->controller)) {
					$this->module = array_shift($uri);
				}
			}
			
			// если не удалось подключить кастомный контроллер,
			// подключаем контроллер по-умолчанию
			if(empty($this->controller)) {
				$this->controller = self::load_class($this->module.'_'.self::$app.'_controller');
			}
			
			// Поиск доступного метода в подключенном контроллере
			if (!empty($uri[0]) && method_exists($this->controller, $uri[0]) && is_callable(array($this->controller, $uri[0]))) {
				$this->action = array_shift($uri);
			} elseif (!method_exists($this->controller, $this->action) || !is_callable(array($this->controller, $this->action))) {
				$this->action = 'page_404';
			}
			
			// осальные части передаем как параметры вызова
			$this->params = (array)$uri;
			
			// сохраняем параметры вызова
			$this->_url->set('module',	$this->module);
			$this->_url->set('action',	$this->action);
			$this->_url->set('params',	$this->params);
		}
		
		public function pre_globals() {
			$this->pre_global_arrays_trim($_POST);
			$this->pre_global_arrays_trim($_GET);
			
			if(get_magic_quotes_gpc()) {
				$this->pre_global_arrays_stripslashes(&$_POST);
				$this->pre_global_arrays_stripslashes(&$_GET);
			}
		}
		
		public function start() {
			
			// âûçûâàåì callback-ìåòîä äî îñíîâíîãî ìåòîäà
			if(method_exists($this->controller, '__before')) {
				$this->controller->__before();
			}
			
			// âûçûâàåì callback-ìåòîä ïîäêëþ÷åíèÿ ðåñóðñîâ
			if(method_exists($this->controller, '__assign_all')) {
				$this->controller->__assign_all();
			}
			
			// îñíîâíîé ìåòîä êîíòðîëëåðà
			call_user_func_array(array($this->controller, $this->action), $this->params);
			
			// âûçûâàåì callback-ìåòîä ïîñëå îñíîâíîãî ìåòîäà
			if(method_exists($this->controller, '__after')) {
				$this->controller->__after();
			}
		}
		
		// ðåêóðñèâíàÿ îáðàáîòêà ýëåìåíòîâ ìàññèâà ïî trim
		public function pre_global_arrays_trim(&$_value) {
			if(is_array($_value)) {
				foreach($_value as $i => &$item) {
					$this->pre_global_arrays_trim($item);
				}
				return true;
			}
			$_value = trim($_value);
			return true;
		}
		
		// ðåêóðñèâíàÿ îáðàáîòêà ýëåìåíòîâ ìàññèâà ïî stripslashes
		public function pre_global_arrays_stripslashes(&$_value) {
			if(is_array($_value)) {
				foreach($_value as $i => &$item) {
					$this->pre_global_arrays_stripslashes($item);
				}
				return true;
			}
			$_value = stripslashes($_value);
			return true;
		}
		
		
		
	}
?>