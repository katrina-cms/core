<?php
	function __autoload($class) {
		// echo 'load: '.$class."\n";
		
		$class = strtolower($class);
		$paths = array();
		// $application_path = defined('APPLICATION_ADMIN') ? APPLICATION_ADMIN : APPLICATION;
		
		// основные контроллеры
		if(preg_match('/^(?P<module>.+)_.+_controller$/i', $class, $matches)) {
			$paths[] = MODULES.$matches['module'].DS;
		}
		
		// контроллеры приложений
		if(preg_match('/^(?P<app>[^_]+)_controller$/i', $class, $matches)) {
			$paths[] = INDEX.$matches['app'].DS;
		}
		
		// модели
		if(preg_match('/^(?P<name>.+)_model$/i', $class, $matches)) {
			$paths[] = MODULES.$matches['name'].DS;
		}
		
		// системные компоненты
		if(preg_match('/^.+_component$/i', $class)) {
			$paths[] = CORE.'components'.DS;
		}
		
		// системные классы
		$paths[] = CORE;
		foreach($paths as $path) {
			if(file_exists($path.$class.'.php')) {
				require_once $path.$class.'.php';
				return;
			}
		}
	}
?>