<?php
	// TODO переписать метод delete
	class file_component extends core_component {

		public function __construct() {
			$this->disallow_extensions = array(
				'php', 'pl', 'exe', 'dll', 'so', 'pu', 'sql', 'js'
			);
		}
		
		/**
		 * Получаем полный путь к файлу
		 * по его относительному пути
		 */
		public function getFullPath($path) {
			$path = explode('/', $path);
			return count($path) == 3 ? MODULES.$path[0].DS.'src'.DS.$path[1].DS.$path[2] : false;
		}
		
		/**
		 * Удаляем файл по его относительному пути
		 */
		public function remove($path) {
			$path = $this->getFullPath($path);
			if(file_exists($path)) {
				return @unlink($path);
			}
			return false;
		}
		
		public function download($info, $upload_path) {
			$file_path = $upload_path.$info['id'].'.'.$info['extension'];
			if(file_exists($file_path)) {
				header("Pragma: public"); 
				header("Expires: 0"); 
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
				header("Cache-Control: private", false);
				header("Content-Type: ".$info['filetype']); 
				header("Content-Disposition: attachment; filename=".str_replace(' ','_',$info['filename']).'.'.$info['extension']); 
				header("Content-Transfer-Encoding: binary"); 
				header("Content-Length: ".$info['filesize']);

				readfile($file_path);
				die();
			}
			$this->url->redirect('::referer');
		}
		
		
		
		public function getFileInfo($filename) {
			if(in_array($this->getExt($filename), $this->disallow_extensions)) return false;
			return array(
				'extension' => $this->getExt($filename),
				'filename'  => $filename
			);
		}
		
		public function wget($filename,$link) {
			$fx = fopen($filename,"w+");
			$file = file_get_contents($link);
			fputs($fx,$file);
			fclose($fx);
		}
		
		public function toFile($file, $content, $mode = 'w+', $attr = 0755, $has_protect = true) {
			if($has_protect && in_array($this->getExt($file), $this->disallow_extensions)) return false;
			$fx = fopen($file, $mode, $attr);
			fputs($fx, $content);
			fclose($fx);
		}
		
		public function ext($file_name) {
			$parts = explode('.', $file_name);
			return $parts[count($parts) - 1];
		}
		
		public function getExt($file_name) {
			return $this->ext($file_name);
		}
	
	}
?>