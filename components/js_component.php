<?php
	class js_component extends core_component {
	
		private $data = array();
	
		public function assign($path, $is_url = false) {
			
			// ���� ��� URL
			if(!empty($is_url)) {
				$this->data[] = array(
					'url'	=> $path
				);
			}
			
			// �������� ���� � ����� �������
			$parts = explode('/', $path);
			$pathes = array();
			
			// ������ ���� � ���������� �������
			if(count($parts) > 1) {
				$pathes[] =
					'/modules/'.$parts[0].'/src/'.
					(self::$app != 'app' ? self::$app.'/' : '').
					'js/'.$parts[1].'.js';
			}
			
			// ������ ���� � ������ �������
			$pathes[] = '/'.self::$app.'/src/js/'.$path.'.js';
			
			// ��������� ���� � �������
			foreach($pathes as $src_path) {
				$abs_path = INDEX.str_replace('/', DS, substr($src_path, 1));
				if(file_exists($abs_path)) {
					$this->data[] = array(
						'src'	=> $src_path,
						'abs'	=> $abs_path
					);
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * ��������� ��� ������� �� �����
		 */
		public function assign_all($dir_path) {
			$parts = explode('/', $dir_path);
			$pathes = array();
			
			// ������ ���� � ���������� �������
			if(count($parts) > 1) {
				$module = array_shift($parts);
				$pathes[] =
					'/modules/'.$module.'/src/'.
					(self::$app != 'app' ? self::$app.'/' : '').
					'js/'.join('/', $parts).'/';
			}
			
			// ������ ���� � ������ �������
			$pathes[] = '/'.self::$app.'/src/js/'.$dir_path.'/';
			
			// ��������� ���� � �������
			foreach($pathes as $rel_path) {
				$abs_path = INDEX.str_replace('/', DS, substr($rel_path, 1));
				if(file_exists($abs_path)) {
					$dir_ptr = dir($abs_path);
					if(empty($dir_ptr)) {
						return true;
					}
					while($file_name = $dir_ptr->read()) {
						$file_path = $abs_path.$file_name;
						if(is_file($file_path)) {
							$this->assign($dir_path.'/'.str_replace('.js', '', $file_name));
						}
						
					}
					return true;
				}
			}
			
			return false;
		}
		
		public function generate() {
			if(empty($this->data)) return '';
			
			$included = array();
			
			$html = '';
			foreach($this->data as $i => &$path) {
				if(!empty($path['url'])) {
					$html .= '<script type="text/javascript" src="'.$path['url'].'"></script>';
					continue;
				}
				if(!in_array($path['src'], $included)) {
					$html .= '<script type="text/javascript" src="'.$path['src'].'?_='.filemtime($path['abs']).'"></script>';
					$included[] = $path['src'];
				}
			}
			
			return $html;
		}
		
	}
?>