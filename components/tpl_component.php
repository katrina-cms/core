<?php
	/**
	 * ���������-������������
	 *
	 * ������ HTML-������� ����� ��������� PHP-������ �� ��������� ���� ������� .tpl,
	 * ������ .tpl-���� �������������� ������������� � .ctpl-����, �������������� �� ���� �������� PHP-��� 
	 *
	 * �������������� �����������:
	 * 	1.	{$label}								- ���������� ����� (� ����� int, float, bool, ...)
	 *
	 * 	2.	{foreach $list as $item} ... {/foreach}	- ������������� ������, ���
	 *		- $item.var1, $item.var2, ...			- ��������� � ���������� ������ �������,
	 *		- $list.pos()							- ��������� ������� ������� ��������
	 *		- $list.even()							- ���� �������� ��������
	 *		- $list.odd()							- ���� ������ ��������
	 *		- $list.first()							- ���� ������ ��������
	 *		- $list.last()							- ���� ��������� ��������
	 *
	 *	3.	{if <...>} ... {/if}					- �������� ����������� ���� <if-then>
	 * 	4.	{if <...>} ... {else} ... {/if}			- �������� ����������� ���� <if-then-else>, ���
	 *		- <...>									- ����� ���������, ���������� � PHP
	 *
	 * �������:
	 * <a href="/view/{$id}/">{$title}</a>
	 * 
	 * <ul id="my_list">
	 *		{foreach $list as $item}
	 *				<li class="{if $list.first()}first{/if}">
	 *				<a href="/show/{$item.id}">{$item.fio}, {$item.email}</a>
	 *			</li>
	 *		{/foreach}
	 * </ul>
	 *
	 * {if !empty($my_var)}
	 *		<a href="#">1</a>
	 * {else}
	 *		<b>2</b>
	 * {/if}
	 */
	class tpl_component extends core_component {
	
		private $glob		= array();	// ���������� ������ �������������
		private $data		= array();	// ������ ��� ��������� �������� ������� (��������� + ����������) 
		private $buffer		= '';		// ����� ����� �������� �������
		
		/**
		 * ����� �������� ������ ��� ���������� � ���������� ���������
		 * ������������ ��� ������ �� �������� ��� � � ���������
		 */
		public function assign($label, $value = null) {
				
			if(!empty($label) && is_array($label) && $value === null) {
				foreach($label as $arr_label => &$arr_value) {
					$this->glob[$arr_label] = $arr_value;
				}
				return true;
			}
			
			if(!empty($label) && !empty($value)) {
				$this->glob[$label] = $value;
				return true;
			}
			
			return false;
		}
		
		/**
		 * ����� ��������� ������ �� ����������� ���������
		 */
		public function get($label) {
			if(isset($this->glob[$label]) && is_string($this->glob[$label])) {
				return $this->glob[$label];
			}
			return false;
		}
		
		/**
		 * ����� ��������� HTML-������ �� ���������� �������
		 * ��� �������������� ��������� ��������� ������ �� ����������
		 *
		 * �������������� �������� @label �������� �� ���������� HTML-������
		 * � ���������� ������� �����
		 */
		public function render($path = false, $data = null, $label = null) {
			
			// �������� ���� � ����� �������
			if(empty($path)) {
				$tpl_parts[0] = $this->url->get('page');
				$tpl_parts[1] = $this->url->get('action');
			} else {
				$tpl_parts	= explode('/', $path);
				if(count($tpl_parts) == 1) {
					$tpl_parts[1] = $tpl_parts[0];
					$tpl_parts[0] = $this->url->get('action');
				}
			}
			
			// ������������ �����
			if($tpl_parts[0] == 'layouts') {
				$tpl_source_path	= INDEX.self::$app.DS.$tpl_parts[0].DS.$tpl_parts[1].'.tpl';
			} else {
				$tpl_source_path	=
					MODULES.$tpl_parts[0].DS.'views'.DS.
					(self::$app != 'app' ? self::$app.DS : '').
					$tpl_parts[1].'.tpl';
			}
			$tpl_cache_path		= CACHE.'tpl'.DS.md5($tpl_source_path).'.ctpl';
			
			// ��������� �� ������� ��������� ����� �������
			if(!file_exists($tpl_source_path)) {
				die($tpl_source_path);
				return false;
			}
			
			// ������������ ����������������� �������
			if(!file_exists($tpl_cache_path) || filemtime($tpl_cache_path) < filemtime($tpl_source_path)) {
			
				$this->buffer = file_get_contents($tpl_source_path);
				// $this->obfuscate($this->buffer);
				$this->buffer_render();
			
				file_put_contents($tpl_cache_path, $this->buffer);
			}
			
			// ������� ����������� � ���������� �������
			$this->data = (!empty($data)) ? array_merge($this->glob, (array)$data) : $this->glob;
			
			// ������
			ob_start();
			require $tpl_cache_path;
			$html = ob_get_clean();
			
			// ���������� �������������� �������
			// $this->obfuscate($html);
			
			// ��������� ���������� �������
			$this->data = array();
			
			// ������������� �������������� �������
			if(!empty($label)) {
				$this->assign($label, $html);
				return true;
			}
			
			// ������� �������������� �������
			return $html;
		}
		
		private function obfuscate(&$text) {
			$text = preg_replace('/\s+/', ' ',  $text);
			$text = str_replace('> <',    '><', $text);
			$text = preg_replace('/\<\!--.+--\>/iuU', '',  $text);
		}
		
		private function buffer_render() {
			
			// ������� ���������� ������ ����� ��������
			$this->buffer = preg_replace(array(
				'/\{foreach (\$[a-z0-9_.]+) as (\$[a-z0-9_]+)\}/ie',	// #1 �������� ����� foreach
				'/\{\/foreach\}/i',										// #2 �������� ����� foreach
				'/\{if (\$[a-z0-9_.]+)\}/ie',							// #3.1 �������� if - ������������� ����������
				'/\{if \!(\$[a-z0-9_.]+)\}/ie',							// #3.1 �������� if - ������������� ����������
				'/\{if (.+?)\}/ie',										// #3 �������� if
				'/\{else\}/i',											// #4 else
				'/\{\/if\}/i',											// #5 �������� if
				'/\{(\$[a-z0-9_.\(\)]+)\}/ie'							// #6 ��������� ����������
			), array(
				'$this->buffer_render_vars(\'$1\', \'<?php if(!empty(%%%) && is_array(%%%) && count(%%%) > 0) { foreach(%%% as \'.str_replace(\'.\', \'_\', \'$2\').\'_iterator => &$2) { ?>\')',
				'<?php } } ?>',
				'$this->buffer_render_vars(\'$1\', \'<?php if(!empty(%%%)) { ?>\')',
				'$this->buffer_render_vars(\'$1\', \'<?php if(empty(%%%)) { ?>\')',
				'$this->buffer_render_vars(\'$1\', \'<?php if(%%%) { ?>\')',
				'<?php } else { ?>',
				'<?php } ?>',
				'$this->buffer_render_vars(\'$1\', \'<?=(isset(%%%)?%%%:\\\'\\\');?>\')'
			), $this->buffer);
			
		}
		
		private function buffer_render_vars($text, $wrap = null, $decode = true) {
			
			$text = preg_replace(array(
				'/(\$([a-z0-9_.]+))\.even\(\)/i',		// #1 �������� �������� �������
				'/(\$([a-z0-9_.]+))\.odd\(\)/i',		// #2 ������ �������� �������
				'/(\$([a-z0-9_.]+))\.pos\(\)/i',		// #3 ������� �������� � �������
				'/(\$([a-z0-9_.]+))\.first\(\)/i',		// #4 ������ ������� � �������
				'/(\$([a-z0-9_.]+))\.last\(\)/ie',		// #5 ��������� ������� � �������
				'/(\$([a-z0-9_.]+))\.([a-z0-9_]+)/i',	// #6 ��������� ������� �������
				'/(\$([a-z0-9_.]+))/i'					// #7 ������� ����������
			), array(
				'##$2_iterator%2==0',
				'##$2_iterator%2==1',
				'##$2_iterator',
				'##$2_iterator==0',
				'$this->buffer_render_vars(\'$1\', \'##\'.str_replace(\'.\', \'_\', \'$2\').\'_iterator==count(%%%)-1\', false)',
				'##$2[\'$3\']',
				'##this->data[\'$2\']'
			), $text);
			
			if($decode) {
				$text = str_replace('##', '$', $text);
			}
			
			return ($wrap) ? str_replace('%%%', $text, $wrap) : $text;
		}
	
	}
?>