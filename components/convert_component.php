<?php
	class convert_component extends core_component {
		
		private $translit = array();
		
		public function __construct() {
			include CONFIG.'translit.config.php';
			$this->translit = $config['translit'];
		}
		
		/**
		 * Конвертируем кириллицу в латиницу
		 */
		public function ru2en($rus_str) {
			return strtr($rus_str, $this->translit['ru']);
		}
		
	}
?>