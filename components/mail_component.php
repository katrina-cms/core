<?php
	class mail_component extends core_component {
		
		private $handler;
		
		public function __construct() {
			require_once PLUGINS.'phpmailer'.DS.'class.phpmailer.php';
			$this->handler = new PHPMailer();
			
			$domain = $this->_config->get('domain', 'site');
			
			$this->handler->IsSMTP();
			$this->handler->Host			= $this->_config->get('host', 'mail');
			$this->handler->SMTPAuth		= true;
			$this->handler->SMTPKeepAlive	= true;
			$this->handler->SMTPSecure		= 'ssl';
			$this->handler->Port			= $this->_config->get('port', 'mail');
			$this->handler->Username		= $this->_config->get('user', 'mail');
			$this->handler->Password		= $this->_config->get('pass', 'mail');
			
			$this->handler->SetFrom(
				$this->_config->get('ebox', 'mail'),
				$this->_config->get('name', 'mail')
			);
			
			$this->handler->Subject			= 'Письмо с сайта '.$domain;
		}
		
		// -- отправить письмо
		public function send($to, $letter, $subject = false, $attaches = false, $config = false) {
			$this->clear();
			
			if (!empty($config) && is_array($config)) {
				foreach ($config as $conf_h => $conf_v) {
					switch ($conf_h) {
						case 'From':
							$this->handler->SetFrom($conf_v[0], $conf_v[1]);
							break;
						case 'ReplyTo':
							$this->handler->ClearReplyTos();
							$this->handler->AddReplyTo($conf_v[0], $conf_v[1]);
							break;
					}
				}
			}
			
			// -- перекрываем тему письма
			if (!empty($subject)) {
				$this->handler->Subject = $subject;
			}
			
			// -- формируем тело письма
			$this->handler->MsgHTML($letter);
			$this->handler->AltBody = strip_tags($letter);
			
			// -- добавляем прикрепленные файлы
			if (!empty($attaches)) {
				if (is_string($attaches)) {
					if (file_exists($attaches)) {
						$this->handler->AddAttachment($attaches);
					}
				} elseif (is_array($attaches)) {
					foreach ($attaches as $name => $path) {
						if (file_exists($path)) {
							$this->handler->AddAttachment($path, $name);
						}
					}
				}
			}
			
			$this->handler->AddAddress($to);
			return $this->handler->Send();
		}
		
		// -- очистить от предыдущей отправки
		private function clear() {
			$this->handler->ClearAddresses();
			$this->handler->ClearAttachments();
		}
		
	}
?>