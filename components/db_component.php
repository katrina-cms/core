<?php
	class db_component extends core_component {
		
		private $db;
		private $host;
		private $user;
		private $pwd;
		private $link;
		
		public function __construct() {
			$this->host = $this->_config->get('host', 'db');
			$this->db   = $this->_config->get('db',   'db');
			$this->user = $this->_config->get('user', 'db');
			$this->pwd  = $this->_config->get('pwd',  'db');
			$this->connect();
		}
		
		private function connect() {
			if(!($this->link = mysql_connect($this->host, $this->user, $this->pwd)) && DEBUG) {
				die('<p>Невозможно соединиться с базой данных</p><p>'.mysql_error().'</p>');
			}
			if(!mysql_select_db($this->db, $this->link) && DEBUG) {
				die('<p>Невозможно выбрать базу данных</p><p>'.mysql_error().'</p>');
			}
			mysql_query('SET NAMES '.$this->_config->get('charset','db')); 
			return $this->link;
		}
		
		public function query($sql) {
			$query = mysql_query($sql, $this->link);
			if(!$query && DEBUG) {
				die('<p><strong>Ошибка в SQL:</strong></p><p>'.$sql.'</p><p>'.mysql_error().'</p>');
			}
			return $query;
		}
		
		public function get_one($sql, $pos = 0) {
			$query = $this->query($sql);
			if(!$query || !mysql_num_rows($query)) {
				return false;
			}
			$result = mysql_fetch_row($query);
			return $result[$pos];
		}
		
		public function get_all($sql) {
			$query = $this->query($sql);
			if(!$query || !mysql_num_rows($query)) {
				return false;
			}
			$result = array();
			while($row = mysql_fetch_assoc($query)) {
				$result[] = $row;
			}
			return $result;
		}
		
		public function get_all_one($sql) {
			$query = $this->query($sql);
			if(!$query || !mysql_num_rows($query)) {
				return false;
			}
			$result = array();
			while($row = mysql_fetch_row($query)) {
				if(isset($row[0])) {
					$result[] = $row[0];
				}
			}
			return $result;
		}
		
		public function get_row($sql, $keys = true) {
			$query = $this->query($sql);
			if(!$query || !mysql_num_rows($query)) {
				return false;
			}
			return ((bool)$keys ? mysql_fetch_assoc($query) : mysql_fetch_row($query));
		}
		
		public function insert($table, $data) {
			$keys  = join(',', array_keys($data));
			$vals  = join(',', $this->escape(array_values($data)));
			$res_q = $this->query('INSERT INTO '.$table.'('.$keys.') VALUES ('.$vals.')');
			return ($res_q ? mysql_insert_id() : false);
		}

		public function update($table, $data, $expr) {
			$data = $this->sql_prepare($data, ',');
			$expr =(is_array($expr))?$this->sql_prepare($expr,' AND '):'id='.$this->escape($expr);
			
			$sql = 'UPDATE '.$table.' SET '.$data.' WHERE ('.$expr.')'; 

			return $this->query($sql);
		}
				
		public function delete($table, $data) {
			if(is_array($data)) {
				if(!count($data)) return false;
				$sql = array();
				foreach($data as $key => &$val) {
					$sql[] = $key.' = '.$this->escape($val);
				}
				$sql = 'DELETE FROM '.$table.' WHERE '.implode(' AND ', $sql);
			} else {
				$sql = 'DELETE FROM '.$table.' WHERE id ='.$data;
			}
			return $this->query($sql);
		}
		
		public function count_rows($sql) {
			$query = $this->query($sql);
			$rows = mysql_num_rows($query);
			return ($rows>0)?$rows:false;
		}
		
		public function mysql_next_id($table) {
			$sql = 'SHOW TABLE STATUS LIKE "'.$table.'"';
			$result = $this->get_row($sql);
			return $result['Auto_increment'];
	}
		
		public function sql_export() {
			$i=0;
			$sql = 'SHOW TABLES';
			$tables = $this->get_all($sql);
			$dump = '';
			while($i<count($tables)) {
				$sql = 'SHOW CREATE TABLE `'.$tables[$i]['Tables_in_'.$this->db].'`';
				$table = $this->get_row($sql,false);
				$dump .= $table[1].";\n\n";
				$insert = $this->get_all('SELECT * FROM '.$tables[$i]['Tables_in_'.$this->db]);
				$k=0;
				while($k<count($insert)) {
					if(!empty($insert[$k])) {
						$keys = join(',',array_keys($insert[$k]));
						$vals = join(',',$this->escape(array_values($insert[$k])));
						$dump .="INSERT INTO ".$tables[$i]['Tables_in_'.$this->db]."(".$keys.") VALUES(".$vals.");\n";
					}
					$k++;
				}
				$dump.="\n";
				$i++;
			}
			return $dump;
		} 
		
		private function sql_prepare($data,$glue) {
			$vals = array();
			foreach($data as $key=>$val) {
				$vals[] = $key.'='.$this->escape($val);
			}
			return join($glue,$vals);
		}
		
		public function escape($str) {
			if(is_array($str)) {
				foreach($str as &$val) {
					$val = $this->escape($val); 
				}
			} 
			return (is_array($str)) ? $str : '"'.mysql_real_escape_string(trim($str)).'"';
		}
		
		public function __toString() {
			return $this->db;
		} 

	}
?>