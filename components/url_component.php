<?php
	class url_component extends core_component {
		
		static $url_vars = array();
		
		// -- ��������������� (2.00a)
		// -- ������ 2.02b:
		// -- 1. ������ �� ���������, �.�. ��������� �� ���� 
		// -- 2. ����������� ��������� ��������� ��������� (@$redirect)
		public function redirect($location, $redirect = '/') {
			if($location == '::referer') {
				$location = $_SERVER[(empty($_SERVER['HTTP_REFERER'])?'REQUEST_URI':'HTTP_REFERER')];
			}
			if($location == $_SERVER['REQUEST_URI']) {
				$location = $redirect;
			}
			header('Location: '.$location);
			die();
		}
		
		public function referer() {
			$this->redirect('::referer');
		}
		
		public function get($key) {
			return self::$url_vars[$key];
		}
		
		public function set($key,$value) {
			self::$url_vars[$key] = $value;  
		}
	}
?>