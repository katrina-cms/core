<?php
	class config_component extends core_component {

		private $data;

		public function __construct() {
			require_once CONFIG.'config.php';
			require_once CONFIG.'config.inc.php';
			
			$this->data = $config;
		}

		public function get($key, $category = 'site') {
			if(empty($category)) return false;
			
			if(array_key_exists($key, $this->data[$category]) === false) {
				return false;
			}
			if(is_string($this->data[$category][$key])) {
				return htmlspecialchars_decode($this->data[$category][$key]);
			}
			return $this->data[$category][$key];
		}

		public function get_array($category) {
			if(false === array_key_exists($category,$this->data)) return false;
			return $this->data[$category];
		}
		
	}
?>