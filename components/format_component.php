<?php
	class format_component extends core_component {
		
		/**
		 * Форматируем стоимость
		 */
		public function cost($cost) {
			$cost = (int)$cost;
			return $cost > 0 ? number_format($cost, 0, ',', ' ').' р.' : false;
		}
		
		/**
		 * Представление в виде телефона
		 */
		public function phone($phone) {
			if(empty($phone)) return '';
			if(strlen($phone) == 12 && $phone[0] == '+') {
				$phone = substr($phone, 1);
			}
			if(strlen($phone) == 11 && ($phone[0] == '7' || $phone[0] == '8')) {
				$phone = substr($phone, 1);
			}
			if(strlen($phone) != 10) {
				return '';
			}
			
			return '+7 ('.
				substr($phone, 0, 3).') '.
				substr($phone, 3, 3).'-'.
				substr($phone, 6, 2).'-'.
				substr($phone, 8, 2);
		}
		
		/**
		 * Формат времени
		 * например продолжительность видео
		 */
		public function time($time) {
			$time = (int)$time;
			if(empty($time)) return '';
			
			$h = (int)floor($time / 3600);
			$m = (int)floor($time / 60) % 60;
			$s = $time % 60;
			
			if($h > 0) {
				return sprintf('%d:%02d:%02d', $h, $m, $s);
			}
			return sprintf('%02d:%02d', $m, $s);
		}
		
		/**
		 * Защита от XSS
		 */
		public function safe_text($text) {
			return strip_tags($text);
		}
		
	}
?>