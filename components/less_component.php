<?php

class less_component extends core_component {
    private $parser;
    private $data = array();

    public function __construct() {
        require_once PLUGINS.'lessphp'.DS.'lessc.inc.php';

        $this->parser = new lessc;
        $this->parser->setImportDir(array(
            '/'.self::$app.'/src/less/lessf/',
            '/'.self::$app.'/src/less/'
        ));
    }

    /**
     * Добавление файла на обработку
     *
     * @param  string  $path Путь до файла
     * @return boolean
     */
    public function assign($path) {
        $parts = explode('/', $path);
        $pathes = array();
        
        // Строим путь к модульному ресурсу
        if (count($parts) > 1) {
            throw new Exception('The less component not supported resources of the modules.', 1);
        }
        
        // Строим путь к общему ресурсу
        $pathes[] = '/'.self::$app.'/src/less/'.$path.'.less';
        
        // Вычисляем путь к ресурсу
        foreach ($pathes as $src_path) {
            $abs_path = INDEX.str_replace('/', DS, substr($src_path, 1));

            if (file_exists($abs_path)) {
                $this->data[] = array(
                    'src'   => $src_path,
                    'abs'   => $abs_path
                );

                return true;
            }
        }

        return false;
    }
    

    /**
     * Обработка LESS файлов
     *
     * @param  boolean $compressed Минификация выходного CSS-кода
     * @return string
     */
    public function generate($compressed = true) {
        if ($compressed) {
            $this->parser->setFormatter('compressed');
        }

        // Файлы и пути
        $cache_file = 'build.temp';
        $build_file = 'build.min.css';
        $paths = array(
            'cache' => CACHE.'css'.DS.$cache_file,
            'build' => array(
                'abs' => CACHE.'css'.DS.$build_file,
                'rel' => '/cache/css/'.$build_file
            )
        );
        $html = '';

        foreach($this->data as $i => &$path) {
            try {
                if (file_exists($paths['cache'])) {
                    $cache = unserialize(file_get_contents($paths['cache']));
                }
                else {
                    $cache = $path['abs'];
                }

                // Обновляем кэш
                $cache_updated = $this->parser->cachedCompile($cache);

                if (!is_array($cache) || $cache_updated['updated'] > $cache['updated']) {
                    file_put_contents($paths['cache'], serialize($cache_updated));
                    file_put_contents($paths['build']['abs'], $cache_updated['compiled']);
                }
            }
            catch (Exception $e) {
                echo 'lessphp fatal error: '.$e->getMessage();
            }

            $html .= '<link rel="stylesheet" type="text/css" href="'.$paths['build']['rel'].'?_='.filemtime($path['abs']).'">';
        }

        return $html;
    }
}