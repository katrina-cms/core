<?php
	class base_component extends core_component {
		
		// -- �������� ��� ������� �� id ������
		public function getTableName($mid) {
			$sql = 'SELECT name FROM module WHERE id = '.(int)$mid;
			return ($mid <= 1) ? 'main' : $this->db->get_one($sql);
		}
		
		// -- �������� �������� ��������� �����������
		public function getFotogallery($pid, $mid) {
			$table = $this->getTableName($mid);
			$sql   = 'SELECT fotogallery FROM '.$table.' WHERE id = '.(int)$pid;
			return (int)$this->db->get_one($sql);
		}
		
		public function del_keys($data, $keys) {
			$arr = $data;
			if(!is_array($keys)) return false;
			foreach($keys as $v) {
				if(isset($arr[$v])) unset($arr[$v]);
			}
			return $arr;
		}
		
		/**
		 * ������� ����������: �����, ��.�����, ��.�.�����, ��.�.�����, ����������� 2��� 10��
		 * �������� ������� ������� + ������� � %d
		 */
		public function declension($count, $f1, $f24, $f50, $is_dec = true) {
			$count = (int)$count;
			if($is_dec && $count > 10 && $count < 20) {
				return is_numeric(strpos($f50, '%d')) ? str_replace('%d', $count, $f50) : $count.' '.$f50;
			}
			if($count % 10 == 1) {
				return is_numeric(strpos($f1, '%d')) ? str_replace('%d', $count, $f1) : $count.' '.$f1;
			}
			if(in_array($count % 10, array(2,3,4))) {
				return is_numeric(strpos($f24, '%d')) ? str_replace('%d', $count, $f24) : $count.' '.$f24;
			}
			return is_numeric(strpos($f50, '%d')) ? str_replace('%d', $count, $f50) : $count.' '.$f50;
		}

	}
?>