<?php
	class alert_component extends core_component {
		
		/**
		 * ��������� ��������� ���� �����������
		 */
		public function info($message, $title = '') {
			$this->set($message, 'info', $title);
		}
		
		/**
		 * ��������� ��������� ���� ������
		 */
		public function error($message, $title = '') {
			$this->set($message, 'error', $title);
		}
		
		/**
		 * ����� ��������� �����������
		 */
		public function generate() {
			$alert = $this->_session->get(self::$app.'_alert');
			if(empty($alert) || empty($alert['message'])) {
				return '';
			}
			$this->_session->del(self::$app.'_alert');
			
			if(self::$app == 'adm') {
				return 
					'<script type="text/javascript">'.
						'$(document).ready(function() { $.jGrowl('.$alert['message'].', { life: 3000}); });'.
					'</script>';
			}
			
			$attr_id = 'pop-up-'.rand();
			if(empty($alert['title'])) {
				$alert['title'] = '�������!';
			}
			
			return $alert;
		}
		
		private function set($message, $type, $title = '') {
			$this->_session->set(self::$app.'_alert', array(
				'message'	=> $message,
				'title'		=> $title,
				'type'		=> $type
			));
		}
		
	}
?>