<?php
	/**
	 * �������� �������� ��� ���� �������� �������
	 * ������������ ��������� ������������� ��������������
	 */
	class core_object {
		
		protected static $classes	= array();	// ����� ������������ ����������� �������
		protected static $app		= 'app';	// �������� �������� ����������

		public function __get($class_name) {
			// echo '-get: '.$class_name."\n";
			
			// �������������� ���������� ������������ ������
			if(preg_match('/^(?P<module>.+)_controller$/i', $class_name, $matches)) {
				$class_name = $matches['module'].'_'.self::$app.'_controller';		// �����������
			} elseif($class_name[0] == '_') {
				$class_name = substr($class_name, 1).'_component';						// ����������
			} else {
				$class_name = $class_name.'_model';										// ������
			}
			
			// ������ ������ �� ���������
			if(!isset(self::$classes[$class_name])) {
				self::$classes[$class_name] = self::load_class($class_name);
			}
			return self::$classes[$class_name];
		}
		
		/**
		 * ���������� ����������� ����� 
		 * ����������� ����� ������� � �������
		 */
		protected static function load_class($class_name) {
			return class_exists($class_name) ? new $class_name() : false;
		}

	}
?>